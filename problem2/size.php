<?php



if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "Something Wrong!!";
    exit();
}

if (!isset($_POST['size'])) {
    echo "Size is Required!!";
    exit();
}
$size = $_POST['size'];

if(!$size) {
    echo "Size is Required!!";
    exit();
}

if (!is_numeric($size)) {
    echo "Size is Must be Numeric!!";
    exit();
}

if ($size < 1 || $size > 8) {
    echo "Size is Must be between 1 & 8!!";
    exit();
}

?>
<html>
    <head>
        <title>Problem 2</title>
    </head>
    <body>
        <div>
            <?php

                

                echo "Please Enter Chessboard Pattern : <br />";
                echo "<form action='output.php' method='post'>";
                for ($i = 0; $i < $size; $i++) {
                    for ($j = 0; $j < $size; $j++) {
                        echo "<input type='text' style='width:35px; height:35px' name='pattern[$i][$j]' />"; 
                    }
                    echo "<br />";
                }
                echo "<br />";
                echo "<input type='hidden' name='size' value='$size' />";
                echo "<input type='submit' value='Go' />";
                echo "</form>";
            ?>
        </div>
    </body>
</html>

