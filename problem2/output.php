<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "Something Wrong!!";
    exit();
}

if (!isset($_POST['size'])) {
    echo "Size is Required!!";
    exit();
}
$size = $_POST['size'];

if(!$size) {
    echo "Size is Required!!";
    exit();
}

if (!is_numeric($size)) {
    echo "Size is Must be Numeric!!";
    exit();
}

if ($size < 1 || $size > 8) {
    echo "Size is Must be between 1 & 8!!";
    exit();
}
/////

if (!isset($_POST['pattern'])) {
    echo "Pattern is Required!!";
    exit();
}
$pattern = $_POST['pattern'];

if(!$pattern) {
    echo "pattern is Required!!";
    exit();
}

if (!is_array($pattern)) {
    echo "Your Input is Invalid!!";
    exit();
}

if (count($pattern) != $size) {
    echo "Your Input is Invalid!!";
    exit();
}


$temp = 'temp';
$output = true;

foreach ($pattern as $rows) {
    if (!is_array($rows)) {
        echo "Your Input is Invalid!!";
        exit();
    }
    
    if (count($rows) != $size) {
        echo "Your Input is Invalid!!";
        exit();
    }
    foreach ($rows as $col) {
        if (!is_numeric($col)) {
            echo "Input Must be 0 or 1!!";
            exit();
        }
        $col = $col + 0;
        if ($col > 1 || $col < 0) {
            echo "Input Must be 0 or 1!!";
            exit();
        }
        if ($temp === 'temp') {
            $temp = $col;
            continue;
        }
        if ($temp == $col) {
            $output = false;
            break 2;
        }
        $temp = $col;
    }
    if ($size % 2 == 0) {
        $temp = $temp ? 0 : 1; 
    }
}

echo $output ? 'True' : 'False';