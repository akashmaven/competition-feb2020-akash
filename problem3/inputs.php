<?php



if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "Something Wrong!!";
    exit();
}

if (!isset($_POST['size'])) {
    echo "Size is Required!!";
    exit();
}
$size = $_POST['size'];

if(!$size) {
    echo "Size is Required!!";
    exit();
}

if (!is_numeric($size)) {
    echo "Size is Must be Numeric!!";
    exit();
}

if ($size < 0) {
    echo "Size is Must be atleast 1!!";
    exit();
}

?>
<html>
    <head>
        <title>Problem 3</title>
    </head>
    <body>
        <div>
            <?php

                

                echo "Please Enter Detail : <br />";
                echo "<form action='output.php' method='post'>";
                echo "<table border='1'>";
                echo "<tr>";
                echo "<td>Name</td>";
                echo "<td>number of question solved until Feb</td>";
                echo "<td>number of question solved until March</td>";
                echo "</tr>";
                for ($j = 0; $j < $size; $j++) {
                    echo "<tr>";
                    echo "<td><input type='text' name='student[$j]' /></td>";
                    echo "<td><input type='text' name='feb[$j]' /></td>";
                    echo "<td><input type='text' name='march[$j]' /></td>";
                    echo "</tr>";
                }
                echo "</table>";
                echo "<input type='hidden' name='size' value='$size' />";
                echo "<input type='submit' value='Go' />";
                echo "</form>";
            ?>
        </div>
    </body>
</html>

